---
title: "Lesson 3"
output:
  html_notebook: default
  pdf_document: default
---
```{r include=FALSE}
# set the seed for random number generation, so that the results are reproducible:
set.seed(123)
```

# Student's tests

## Two samples test

Let's create to samples normally distributed representing the alcoholic content of two brewing lines:

```{r}
n1 <- 10
n2 <- 12
nom <- 5.6
s1 <- rnorm(n1, nom, 0.05)
s2 <- rnorm(n2, nom+0.1, 0.05)
```
We can visualize treatments with the `boxplot()` function:

```{r}
boxplot(s1, s2, horizontal=T, names=c("Line 1", "Line 2"), xlab="Alcohol (%)")
```

We can observe that the two samples have significantly different sample means, and we are tempted to reject the null hypothesis. We want to verify this with a Student's T-test.

```{r}
(t.test(s1, s2, alt="two.sided", var.equal = T) -> test1)
```
Give that the p-value of the test is $`r test1$p.value`$ we can safely reject the null hypothesis.


## One sample test

Let's suppose that we want to check wether the alcholic content of `s1` is in line with the expected value of 5.6%.
```{r}
(t.test(s1, mu=nom) -> test2)
```
Now the p-value is $`r round(test2$p.value, 3)*100`$%, so we cannot reject the null hypothesis.

If we want to test that the sample value is actually larger than a given reference value, we can use the one side test:

```{r}
(t.test(s1, mu=nom, alt="great") -> test3)
```
It shows that the observed sample average of $`r test3$estimate`$ has a probability of $`r round(test3$p.val, 3)*100`$% of resulting from a population with $`r nom`$% expected value, which is too large for rejecting the null hypothesis, but still lesser than the p-value of the previous two-side test.
